<?php
    
// EDIT THE 2 LINES BELOW AS REQUIRED
$email_to = "asksga@sga.ua.edu";
$email_subject = "Service Request from Ask UA SGA";

// Will be true when there's an error with the form data
$is_form_error = false;

// Get/sanitize form input
// @TODO Is it setup to notify the user if their info doesn't pass sanitization?
$name = strip_tags( trim( $_POST['name'] ) ); // required
$email_from = filter_var( trim( $_POST['email'] ), FILTER_VALIDATE_EMAIL ); // required
$department = strip_tags( trim( $_POST['department'] ) ); // required
$message = strip_tags( trim( $_POST['message'] ) ); // required

// Make sure we have a valid email address
if ( ! $email_from ) {
	$is_form_error = true;
}

// @TODO If you want to display errors, you can use the following code to send an error to your AJAX call
/*if ( $is_form_error ) {
	// header('HTTP/1.1 500 Internal Server Error');
	// die( json_encode( array( 'message' => 'include error message here' )) );
}*/

// Adjust the email to according to the department they select
switch( $department ) {
    
    case 'Academics':
    case 'Act Card':
    case 'Dining':
    case 'FAC':
    case 'Facilities':
    case 'Health':
    default:
    	$email_to = "asksga@sga.ua.edu";
    	break;

}

// Build email message
$email_message = "Form details below.\n\n";
$email_message .= "Name: ".$name."\n";
$email_message .= "Email: ".$email_from."\n";
$email_message .= "Department: ".$department."\n";
$email_message .= "Comments: ".$message."\n";

// Create email header
//$headers = 'Comments/ Request From: '.$email_from."\r\n" . 'X-Mailer: PHP/' . phpversion();  
$header = "From: {$email_from}\r\n"; 

// Send notification email to department
mail( $email_to, $email_subject, $email_message, $header );  

// Setup response email
$response_subject = "Thanks for asking SGA!";
$response_from_email = 'asksga@sga.ua.edu';
//$response_header  = 'MIME-Version: 1.0' . "\r\n";
$response_header .= "From: The University of Alabama Student Government Association <{$response_from_email}>\r\n"; 
$response_header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

// Build response message
$response_message = "<html>
    <head></head> 
    <body>
        <div class='header' style='width:100%;background-color:#A51E36;color:#fff;text-align:center;padding:5px;box-sizing:border-box;'>
            <img src='https://sga.ua.edu/static/ua-square.png' alt='UA SGA' style='width:20%;max-width:70px;'/>
            <h1 style='margin:5px;'>THANKS FOR ASKING SGA!</h1>
        </div>
        <div style='border-top:1px solid #ccc;border-bottom: 1px solid #ccc;background-color: #eee;padding:10px;font-size:1.2em;box-sizing:border-box;'>
            <p style='color:#222;'>Thank you for reaching out to your Student Government Association. Your message has been received and we are working diligently to find an answer. You should expect to hear back from us within the next 48 hours. Roll Tide!</p>
            <p style='color:#222;'>Thanks,</p>
            <p style='color:#222;'><em>Your student government team</em></p>
        </div>  
        <div class='footer' style='text-align:center;box-sizing:border-box;'>
            <h3 style='margin:5px 0px 5px 0px;font-weight:600;color:#222;'>Follow us on social media</h3>
            <div class='social-media-icons'>
                <a href='https://www.facebook.com/UASGA' style='text-decoration:none;'>
                    <img src='https://sga.ua.edu/static/facebook-crimson.png' alt='Facebook' style='display:inline;width:20%;max-width:60px;'/>
                </a>
                <a href='https://twitter.com/uasga' style='text-decoration:none;'>
                    <img src='https://sga.ua.edu/static/twitter-crimson.png' alt='Twitter'style='display:inline;width:20%;max-width:60px;'/>
                </a>
                <a href='https://instagram.com/uasga/' style='text-decoration:none;'>
                    <img src='https://sga.ua.edu/static/instagram-crimson.png' alt='Instagram'style='display:inline;width:20%;max-width:60px;'/>
                </a>
            </div>
            <p style='color:#222;'><em>The University of Alabama Student Government Association</em></p>
            <p style='color:#222;'><em>You received this e-mail because someone provided this address at 
                <a href='sga.ua.edu/asksga/' style='text-decoration:none;color:#A51E36;'>sga.ua.edu/asksga</a>.
                If you received this e-mail in error, please contact 
                <a href='mailto:webmaster@sga.ua.edu' style='text-decoration:none;color:#A51E36;'>webmaster@sga.ua.edu</a></em>
            </p>
        </div>
        <div style='width:100%;box-sizing:border-box;height:30px;background-color:#A51E36;'>&nbsp;</div>
    </body>
</html>
";

// Send response email
mail($email_from, $response_subject, $response_message, $response_header);
