$(document).ready(function() {
   $("#full-navigation-wrapper").hide();
   $("#full-resource-wrapper").hide();
});

$("#home-tab").click(function(){
    if ($("#directory").hasClass("active")) {
        $("#full-navigation-wrapper").slideToggle();
        $(this).addClass('active');
        $("#directory").removeClass('active');
    }
    if ($("#resources").hasClass("active")) {
        $("#full-resource-wrapper").slideToggle();
        $(this).addClass('active');
        $("#resources").removeClass('active');
    }
});

$("#directory").click(function(){
    $("#full-navigation-wrapper").slideToggle();
    if ($(this).hasClass("active")) {
        $("#home-tab").addClass("active");
        $(this).removeClass("active");
    } 
    else if ($("#resources").hasClass("active")) {
        $("#home-tab").removeClass("active");
        $("#resources").removeClass("active");
        $("#full-resource-wrapper").slideToggle();
        $(this).addClass("active");
    }
    else {
        $("#home-tab").removeClass("active");
        $("#resources").removeClass("active");
        $(this).addClass("active");
    }
});

$("#resources").click(function(){
    $("#full-resource-wrapper").slideToggle();
    if ($(this).hasClass("active")) {
        $("#home-tab").addClass("active");
        $(this).removeClass("active");
    }
    else if ($("#directory").hasClass("active")) {
        $("#home-tab").removeClass("active");
        $("#directory").removeClass("active");
        $("#full-navigation-wrapper").slideToggle();
        $(this).addClass("active");
    }
    else{
        $("#home-tab").removeClass("active");
        $("#directory").removeClass("active");
        $(this).addClass("active");
    }
});