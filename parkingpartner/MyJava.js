function switchVisible() {
    if (document.getElementById('Div1')) {
        if (document.getElementById('Div1').style.display == 'none') {
            document.getElementById('Div1').style.display = 'block';
            document.getElementById('Div2').style.display = 'none';
        } 
        else{
            document.getElementById('Div1').style.display = 'none';
            document.getElementById('Div2').style.display = 'block';
        }
    }
}

function switchVisibleErase() {

    if (document.getElementById('Div1')) {

        var timeClass = "";
        var locationClass = "";

        if (document.getElementById('Div1').style.display == 'none') {
            document.getElementById('Div1').style.display = 'block';
            document.getElementById('Div2').style.display = 'none';
        } else {
            document.getElementById('Div1').style.display = 'none';
            document.getElementById('Div2').style.display = 'block';
        }
    }
}

function displayResults() {
    var locationClass = document.myClassLocationForm.LocationOfClass.value;
    if (locationClass == "AdamsHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 9 minutes <br />" +
           "Maximum Walk Time to Class: 12 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
         "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 12pm. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Southeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 15 minutes <br />" +
           "Maximum Walk Time to Class: 18 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/South-East-Commuter-6-23-15.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "AIME") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 10 minutes <br />" +
           "Maximum Walk Time to Class: 19 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 14 minutes <br />" +
           "Maximum Walk Time to Class: 24 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
    else if (locationClass === "BBComerHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 5 minutes <br />" +
           "Maximum Walk Time to Class: 12 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 13 minutes <br />" +
           "Maximum Walk Time to Class: 23 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
    else if (locationClass === "BevillBuilding") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 12 minutes <br />" +
           "Maximum Walk Time to Class: 20 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 14 minutes <br />" +
           "Maximum Walk Time to Class: 24 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "BidgoodHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 5 minutes <br />" +
           "Maximum Walk Time to Class: 8 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 15 minutes <br />" +
           "Maximum Walk Time to Class: 25 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
        else if (locationClass === "BiologyBuilding") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
            "Minimum Walk Time to Class: 7 minutes <br />" +
            "Maximum Walk Time to Class: 17 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    } else if (locationClass === "CapstoneCollege") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Perimeter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Walk Time to Class: 5 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/Perimeter-Parking-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
        else if (locationClass === "CarmichaelHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 5 minutes <br />" +
           "Maximum Walk Time to Class: 8 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass"+
        "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 15 minutes <br />" +
           "Maximum Walk Time to Class: 25 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
    else if (locationClass === "ClarkHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 7 minutes <br />" +
           "Maximum Walk Time to Class: 12 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
                    "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of the lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 12 minutes <br />" +
           "Maximum Walk Time to Class: 22 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
    else if (locationClass === "DosterHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 9 minutes <br />" +
           "Maximum Walk Time to Class: 11 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 15 minutes <br />" +
           "Maximum Walk Time to Class: 27 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
   }

    else if (locationClass === "FarrahHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 9 minutes <br />" +
           "Maximum Walk Time to Class: 21 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "MoodyMusicBuilding") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Southeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 2 minutes <br />" +
           "Maximum Walk Time to Class: 10 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/South-East-Commuter-6-23-15.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
    else if (locationClass === "GallaleeHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 9 minutes <br />" +
           "Maximum Walk Time to Class: 21 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "GarlandHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 8 minutes <br />" +
           "Maximum Walk Time to Class: 14 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of the lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 10 minutes <br />" +
           "Maximum Walk Time to Class: 20 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";

    }

    else if (locationClass === "GordonPalmerHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 8 minutes <br />" +
           "Maximum Walk Time to Class: 18 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "GravesHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 6 minutes <br />" +
           "Maximum Walk Time to Class: 8 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of the lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 14 minutes <br />" +
           "Maximum Walk Time to Class: 21 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "HardawayHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 8 minutes <br />" +
           "Maximum Walk Time to Class: 14 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of the lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 11 minutes <br />" +
           "Maximum Walk Time to Class: 21 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
 
    else if (locationClass === "HMComerHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 9 minutes <br />" +
           "Maximum Walk Time to Class: 17 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of the lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 11 minutes <br />" +
           "Maximum Walk Time to Class: 21 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "HouserHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 10 minutes <br />" +
           "Maximum Walk Time to Class: 20 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "LawCenter") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Southeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 3 minutes <br />" +
           "Maximum Walk Time to Class: 8 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/South-East-Commuter-6-23-15.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "LittleHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 11 minutes <br />" +
           "Maximum Walk Time to Class: 23 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "OsbandHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Southeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 9 minutes <br />" +
           "Maximum Walk Time to Class: 12 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/South-East-Commuter-6-23-15.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "RussellHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 7 minutes <br />" +
           "Maximum Walk Time to Class: 19 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "SERC") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 10 minutes <br />" +
           "Maximum Walk Time to Class: 19 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "NottHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>Northeast Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 9 minutes <br />" +
           "Maximum Walk Time to Class: 21 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "ManlyHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 8 minutes <br />" +
           "Maximum Walk Time to Class: 12 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
               "<br />" +
                             "<br />" +
                                           "- - - - - - - - <br />" +
                "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of the lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
           "<br />" +
          "Minimum Walk Time to Class: 12 minutes <br />" +
          "Maximum Walk Time to Class: 19 minutes <br />" +
                      "<br />" +

          "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "MaryHewellAlstonHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 4 minutes <br />" +
           "Maximum Walk Time to Class: 8 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of the lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 16 minutes <br />" +
           "Maximum Walk Time to Class: 26 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";

    }

    else if (locationClass === "MorganHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 5 minutes <br />" +
           "Maximum Walk Time to Class: 10 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 13 minutes <br />" +
           "Maximum Walk Time to Class: 23 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "NERC") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 11 minutes <br />" +
           "Maximum Walk Time to Class: 19 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 13 minutes <br />" +
           "Maximum Walk Time to Class: 23 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";

    }

    else if (locationClass === "PatyHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 8 minutes <br />" +
           "Maximum Walk Time to Class: 17 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. To ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 15 minutes <br />" +
           "Maximum Walk Time to Class: 25 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";

    }

    else if (locationClass === "ReesePhiferHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 8 minutes <br />" +
           "Maximum Walk Time to Class: 10 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 17 minutes <br />" +
           "Maximum Walk Time to Class: 29 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";

    }

    else if (locationClass === "tenHoorHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 3 minutes <br />" +
           "Maximum Walk Time to Class: 10 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 16 minutes <br />" +
           "Maximum Walk Time to Class: 25 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }

    else if (locationClass === "WoodsHall") {
        document.getElementById("demo").innerHTML =

                       "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>West Commuter!</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: 7 minutes <br />" +
           "Maximum Walk Time to Class: 13 minutes <br />" +
                       "<br />" +
                                              "<br />" +
           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!"+
            "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of these lots are usually full by 11am. If you start after this time, to ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>Northeast Commuter <br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: 11 minutes <br />" +
           "Maximum Walk Time to Class: 21 minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/NE-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";

    }
    else {
        document.getElementById("demo").innerHTML = "Whoops! Looks like there was an error. Please try again.";
    }
}
