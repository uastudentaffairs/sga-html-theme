function switchVisible() {
    if (document.getElementById('Div1')) {
        if (document.getElementById('Div1').style.display == 'none') {
            document.getElementById('Div1').style.display = 'block';
            document.getElementById('Div2').style.display = 'none';
        } 
        else{
            document.getElementById('Div1').style.display = 'none';
            document.getElementById('Div2').style.display = 'block';
        }
    }
}

function switchVisibleErase() {

    if (document.getElementById('Div1')) {

        var timeClass = "";
        var locationClass = "";

        if (document.getElementById('Div1').style.display == 'none') {
            document.getElementById('Div1').style.display = 'block';
            document.getElementById('Div2').style.display = 'none';
        } else {
            document.getElementById('Div1').style.display = 'none';
            document.getElementById('Div2').style.display = 'block';
        }
    }
}

function displayResults() {
    var timeClass = document.myClassTimeForm.timeOfClass.value;
    var locationClass = document.myClassLocationForm.LocationOfClass.value;
    
    locationDict = {
    AdamsHall:["West Commuter","9","12",11,"Southeast Commuter","15","18",15,"South-East-Commuter-6-23-15"],
    AIME:["West Commuter","10","19",11,"Northeast Commuter","14","24",15,"NE-Commuter-3-13-20151"],
    BBComerHall:["West Commuter","5","12",11,"Northeast Commuter","13","23",15,"NE-Commuter-3-13-20151"],
    BevillBuilding:["West Commuter","12","20",11,"Northeast Commuter","14","24",15,"NE-Commuter-3-13-20151"],
    BidgoodHall:["West Commuter","5","8",11,"Northeast Commuter","15","25",15,"NE-Commuter-3-13-20151"],
    BiologyBuilding:["Northeast Commuter","7","17",99,"","","",99,"NE-Commuter-3-13-20151"],
    CapstoneCollege:["Perimeter","5","5",99,"","","",99,"Perimeter-Parking-3-13-20151"],
    CarmichaelHall:["West Commuter","5","8",11,"Northeast Commuter","15","25",15,"NE-Commuter-3-13-20151"],
    ClarkHall:["West Commuter","7","12",11,"Northeast Commuter","12","22",15,"NE-Commuter-3-13-20151"],
    DosterHall:["West Commuter","9","11",11,"Northeast Commuter","15","27",15,"NE-Commuter-3-13-20151"],
    FarrahHall:["Northeast Commuter","9","21",99,"","","",99,"NE-Commuter-3-13-20151"],
    MoodyMusicBuilding:["Southeast Commuter","2","10",99,"","","",99,"South-East-Commuter-6-23-15"],
    GallaleeHall:["Northeast Commuter","9","21",99,"","","",99,"NE-Commuter-3-13-20151"],
    GarlandHall:["West Commuter","8","14",11,"Northeast Commuter","10","20",15,"NE-Commuter-3-13-20151"],
    GordonPalmerHall:["Northeast Commuter","8","18",99,"","","",99,"NE-Commuter-3-13-20151"],
    GravesHall:["West Commuter","6","8",11,"Northeast Commuter","14","21",15,"NE-Commuter-3-13-20151"],
    HardawayHall:["West Commuter","8","14",11,"Northeast Commuter","11","21",15,"NE-Commuter-3-13-20151"],
    HMComerHall:["West Commuter","9","17",11,"Northeast Commuter","11","21",15,"NE-Commuter-3-13-20151"],
    HouserHall:["Northeast Commuter","10","20",99,"","","",99,"NE-Commuter-3-13-20151"],
    LawCenter:["Southeast Commuter","3","8",99,"","","",99,"South-East-Commuter-6-23-15"],
    LittleHall:["Northeast Commuter","11","23",99,"","","",99,"NE-Commuter-3-13-20151"],
    OsbandHall:["Southeast Commuter","9","12",99,"","","",99,"South-East-Commuter-6-23-15"],
    RussellHall:["Northeast Commuter","7","19",99,"","","",99,"NE-Commuter-3-13-20151"],
    SERC:["Northeast Commuter","10","19",99,"","","",99,"NE-Commuter-3-13-20151"],
    NottHall:["Northeast Commuter","9","21",99,"","","",99,"NE-Commuter-3-13-20151"],
    ManlyHall:["West Commuter","8","12",11,"Northeast Commuter","12","19",15,"NE-Commuter-3-13-20151"],
    MaryHewellAlstonHall:["West Commuter","4","8",11,"Northeast Commuter","16","26",15,"NE-Commuter-3-13-20151"],
    MorganHall:["West Commuter","5","10",11,"Northeast Commuter","13","23",15,"NE-Commuter-3-13-20151"],
    NERC:["West Commuter","11","19",11,"Northeast Commuter","13","23",15,"NE-Commuter-3-13-20151"],
    PatyHall:["West Commuter","8","17",11,"Northeast Commuter","15","25",15,"NE-Commuter-3-13-20151"],
    ReesePhiferHall:["West Commuter","8","10",11,"Northeast Commuter","17","29",15,"NE-Commuter-3-13-20151"],
    tenHoorHall:["West Commuter","3","10",11,"Northeast Commuter","16","25",15,"NE-Commuter-3-13-20151"],
    WoodsHall:["West Commuter","7","13",11,"Northeast Commuter","11","21",15,"NE-Commuter-3-13-20151"]
    };
    /*  0: Main Lot
        1: Main Lot Min Walk Time
        2: Main Lot Max Walk Time
        3: Hour that main lot reaches half capacity
        4: Backup Lot
        5: Backup Min Walk Time
        6: Backup Max Walk Time
        7: Hour that main lot reaches full capacity
        8: URL Ending
    */
    
    if(timeClass >= locationDict[locationClass][7]){
        document.getElementById("demo").innerHTML=
                "<h3>The best parking pass for you is:</h3>" +
            "<span style='font-size:3em'>"+locationDict[locationClass][4]+"</span><br />" +
                                   "<br />" +
                                   "<br />" +
                 "Although the West Commuter lots are closest, the lots are usually full by your class time. In order to ensure you have a parking space, it may be best to consider this pass." +
            "<br />" +
                        "<br />" +
           "Minimum Walk Time to Class: "+locationDict[locationClass][5]+" minutes <br />" +
           "Maximum Walk Time to Class: "+locationDict[locationClass][6]+" minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/"+locationDict[locationClass][8]+".pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
    else if(timeClass >= locationDict[locationClass][3]){
        document.getElementById("demo").innerHTML =
                       "<h3>The best parking pass for you is:</h3>" +
                "<span style='font-size:3em'>"+locationDict[locationClass][0]+"</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: "+ locationDict[locationClass][1] +" minutes <br />" +
           "Maximum Walk Time to Class: "+ locationDict[locationClass][2] +" minutes <br />" +
                       "<br />" +
          "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!" +
        "<br />" +
                "<br />" +
                              "<br />" +
                                            "- - - - - - - - <br />" +
                 "<h3>NOTE:</h3> Although the West Commuter lots are closest, half of the lots are usually full by your class time. To ensure you have a parking space, consider: " +
                 "<span style='font-size:1.5em'>"+locationDict[locationClass][4] +"<br /></span>" +
            "<br />" +
           "Minimum Walk Time to Class: "+ locationDict[locationClass][5] +" minutes <br />" +
           "Maximum Walk Time to Class: "+ locationDict[locationClass][6] +" minutes <br />" +
                       "<br />" +

           "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/"+locationDict[locationClass][8]+".pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
    else if(locationDict[locationClass][0] == "West Commuter"){
        document.getElementById("demo").innerHTML =
            "<h3>The best parking pass for you is:</h3>" +
                "<span style='font-size:3em'>"+locationDict[locationClass][0]+"</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: "+ locationDict[locationClass][1] +" minutes <br />" +
           "Maximum Walk Time to Class: "+ locationDict[locationClass][2] +" minutes <br />" +
                       "<br />" +
          "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/West-Commuter-3-13-20151.pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
    
    else{
        document.getElementById("demo").innerHTML =
            "<h3>The best parking pass for you is:</h3>" +
                "<span style='font-size:3em'>"+locationDict[locationClass][0]+"</span><br />" +
                                   "<br />" +
                                   "<br />" +
           "Minimum Walk Time to Class: "+ locationDict[locationClass][1] +" minutes <br />" +
           "Maximum Walk Time to Class: "+ locationDict[locationClass][2] +" minutes <br />" +
                       "<br />" +
          "Click <a href='http://bamaparking.ua.edu/wp-content/uploads/sites/3/2013/08/"+locationDict[locationClass][8]+".pdf'>here</a> to see the map of parking spaces available with this pass!";
    }
}
